#pragma once

#include "cloud2d.h"
#include "cloud2d_trajectory.h"
#include "cloud2d_trajectory_merger.h"
#include "aligner2d.h"
#include "simple_graph.h"
#include "merger_vertices_buffer.h"
#include "condensed_graph_creator.h"

namespace srrg_mapper2d {

  struct VertexIDComparator {
    bool operator() (const OptimizableGraph::Vertex* v1, const OptimizableGraph::Vertex* v2) const
    {
      return v1->id() < v2->id();
    }
  };

  typedef std::set<OptimizableGraph::Vertex*, VertexIDComparator> OrderedVertexSet;
  
  class GraphMerger{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    GraphMerger(SimpleGraph* sgraph);

    void merge(VertexSE2* reference_vertex, VertexSE2* other_vertex);
    void merge(OrderedVertexSet& vertices);
    bool alreadyInQueue(OptimizableGraph::VertexSet& vertices);
    void addVertexToMerge(VertexSE2* currentv);
    void addVerticesToMerge(OptimizableGraph::VertexSet& vertices);
    bool triggerMerge();

    inline std::vector<OptimizableGraph::VertexSet> verticesToMerge() {return _vertices_to_merge.vset();}

    void computeNewNeighbors(OptimizableGraph::VertexSet& vertices_to_remove, OptimizableGraph::VertexSet& vertices_to_connect);

    void connectNeighbors(OptimizableGraph::Vertex* reference_vertex, OptimizableGraph::VertexSet& neighbors);
    void removeMergedVertices(OptimizableGraph::VertexSet& vertices_to_remove);


    inline void setVertexMergingDistance(float vertex_merging_distance){_vertex_merging_distance = vertex_merging_distance;}
    inline void setMergingDistance(float merging_distance){_c2DMerger->setMergingDistance(merging_distance);}
    inline void setProjectiveMerge(bool projective_merge){_c2DMerger->setProjectiveMerge(projective_merge);}
    inline void setVoxelizeResolution(float voxelizeResolution){_c2DMerger->setVoxelizeResolution(voxelizeResolution);}

    inline void setGraphMap(SimpleGraph* sgraph){_sgraph = sgraph;}
    inline void setCloudMergerProjector(Projector2D* projector) {_c2DMerger->setProjector(projector);}

  protected:
    float _vertex_merging_distance;

    MergerVerticesBuffer _vertices_to_merge;
    
    SimpleGraph* _sgraph;
    Projector2D* _projector;
    Cloud2DAligner* _c2DAligner;
    Cloud2DWithTrajectoryMerger* _c2DMerger;

    //Aligner parameters
    float _inliers_distance;
    float _min_inliers_ratio;
    int _min_num_correspondences;
  };

}
