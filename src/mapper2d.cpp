#include "mapper2d.h"

namespace srrg_mapper2d {

  Mapper2D::Mapper2D(LaserMessageTracker* tracker, SimpleGraph* graph_map): _gmerger(0) {
    _tracker = tracker;
    _graph_map = graph_map;

    _previous_vertex_tracker_t = Eigen::Isometry2f::Identity();
    _previous_tracker_t = Eigen::Isometry2f::Identity();

    _ca.init();
    _pa.init();
    
    _track_new_cloud = true;
    _current_cloud_trajectory = NULL;

    _first_vertex = true;
    _multi_session = false;
    _multi_session_started = false;
    _force_display = false;
    _log_times = false;
    _log_stream = 0;
  }

  bool Mapper2D::compute(LaserMessage* las){
    _force_display = false;

    if (!las)
      return false;

    if (las->ranges().size()<100)
      return false;
    
    if (_first_vertex) {
      std::cerr << "Need to call init first" << std::endl;
      return false;
    }

    
    //Add laser message to tracker
    if (!_tracker->compute(las)){
      //Increment rough estimate with odometry 
      Eigen::Isometry2f pa_odom = toIsometry2f(las->odometry());
      _pa.setOdom(pa_odom);
      _pa.estimate();
      
      return false;
    }

    //Check displacement transform and covariance
    Eigen::Isometry2f global_t = _tracker->globalT();
    Eigen::Isometry2f relative_tracker_t = _previous_tracker_t.inverse() * global_t;
    Eigen::Matrix3f cov = _tracker->solver()->informationMatrix().inverse();
    _ca.Oplus(relative_tracker_t, cov);
    _previous_tracker_t = global_t;

    //Managing cloud with trajectory waypoints
    if (_track_new_cloud){
      _current_cloud_trajectory = new Cloud2DWithTrajectory;
      _current_cloud_trajectory->setPose(global_t);
      LaserMessage* las2 = new LaserMessage;
      *las2 = *las;
      _current_cloud_trajectory->pushWaypoint(las2); //stores original laserscan (debugging)
      _track_new_cloud = false;
    } else {
      Eigen::Isometry2f last_pushed_pose = _current_cloud_trajectory->lastPushedPose();
      Eigen::Isometry2f delta_pushed = last_pushed_pose.inverse() * global_t;
      
      float waypoint_translation_threshold = 0.1;
      if (delta_pushed.translation().norm() > waypoint_translation_threshold) {
	_current_cloud_trajectory->setPose(global_t);
	LaserMessage* las2 = new LaserMessage;
	*las2 = *las;
	_current_cloud_trajectory->pushWaypoint(las2);
      } 
    }

    Tracker2D::Status status = _tracker->status();
    Eigen::Isometry2f delta_t = _previous_vertex_tracker_t.inverse() * global_t;
    Eigen::Vector3f delta_tv = t2v(delta_t);
    //Check if we add a new vertex into the graph
    if (delta_t.translation().norm() > _vertex_translation_threshold || 
	fabs(delta_tv.z()) > _vertex_rotation_threshold || 
	status!=Tracker2D::OK){

      //Preparing cloud final data
      _current_cloud_trajectory->add(*_tracker->reference());
      _current_cloud_trajectory->setPose(global_t);
      LaserMessage* las2 = new LaserMessage;
      *las2 = *las;
      _current_cloud_trajectory->pushWaypoint(las2);
      
      VertexSE2* previousVertex = _graph_map->currentVertex();
      
      //Place new vertex in the graph
      Vector3f pose = t2v(global_t);
      _graph_map->addVertex(pose, _current_cloud_trajectory);
      std::cerr << "Adding vertex: " << _graph_map->currentVertex()->id() << endl;

      _graph_map->addEllipseData(_graph_map->currentVertex());

      //Adding edge connecting previous vertex
      if (status == Tracker2D::OK){
	//Get edge covariance from accumulator
	Eigen::Matrix3d inf = _ca.P().inverse().cast<double>();
	SE2 relative_transf(delta_t.cast<double>());
	_graph_map->addEdge(previousVertex, _graph_map->currentVertex(), relative_transf, inf);
      }else{
	//Adding vertex was due to some error, we prepare a more weak edge
	Eigen::Matrix3d inf = 500*Eigen::Matrix3d::Identity();
	SE2 relative_transf(delta_t.cast<double>());
	_graph_map->addEdge(previousVertex, _graph_map->currentVertex(), relative_transf, inf);
      }

      //Trying to get some closures
      std::chrono::steady_clock::time_point time_matching_start = std::chrono::steady_clock::now();
      _graph_map->matchVertex(_graph_map->currentVertex());
      std::chrono::steady_clock::time_point time_matching_end = std::chrono::steady_clock::now();
      if (_verbose){
	std::cerr << "Matching Vertex took: "
		  << std::chrono::duration_cast<std::chrono::milliseconds>(time_matching_end - time_matching_start).count() << " ms" << std::endl;
      }

      //Optimizing
      std::chrono::steady_clock::time_point time_optimization_start = std::chrono::steady_clock::now();
      _graph_map->optimize(10);
      std::chrono::steady_clock::time_point time_optimization_end = std::chrono::steady_clock::now();
      if (_verbose){
	std::cerr << "Optimizing Graph took: "
		  << std::chrono::duration_cast<std::chrono::microseconds>(time_optimization_end - time_optimization_start).count()*1e-3 << " ms" << std::endl;
      }
      if (_log_times){
	*(_log_stream) << std::chrono::duration_cast<std::chrono::microseconds>(time_optimization_end - time_optimization_start).count()*1e-3 << std::endl;
      }

      //Pruning previous vertex cloud. Need to be done with graph optimized
      Cloud2DWithTrajectory* cloudPreviousVertex = (Cloud2DWithTrajectory*)_graph_map->vertexCloud(previousVertex);

      _cmerger.setReference(cloudPreviousVertex);
      _cmerger.setCurrent(_current_cloud_trajectory);
      _cmerger.setProjector(_tracker->projector());
      _cmerger.prune();
      *cloudPreviousVertex = *_cmerger.cloudMerged();

      //Merging clouds
      if (_use_merger){
	std::cerr << std::endl << "MERGER" << std::endl;
	_gmerger.setGraphMap(_graph_map);
	_gmerger.setCloudMergerProjector(_tracker->projector());

	if (_graph_map->verticesToMerge().size()){
	  for (std::set<OptimizableGraph::VertexSet>::iterator itvset = _graph_map->verticesToMerge().begin(); itvset != _graph_map->verticesToMerge().end(); itvset++){
	    OptimizableGraph::VertexSet vset = *itvset;
	    _gmerger.addVerticesToMerge(vset);
	  }
	}
	_force_display = _gmerger.triggerMerge();
      }

      _graph_map->updateEllipseData();
      
      _previous_vertex_tracker_t = global_t;
      _track_new_cloud = true;
      _ca.init();

      //Set estimate in pose accumulator
      Eigen::Isometry2f pa_estimate = _graph_map->currentVertex()->estimate().toIsometry().cast<float>();
      Eigen::Isometry2f pa_tracker = global_t;
      Eigen::Isometry2f pa_odom = toIsometry2f(las->odometry());
      _pa.setEstimate(pa_estimate, pa_tracker, pa_odom);
      _pa.estimate();
    } else {
      //Increment rough estimate with tracker pose 
      Eigen::Isometry2f pa_tracker = global_t;
      Eigen::Isometry2f pa_odom = toIsometry2f(las->odometry());
      _pa.setTracker(pa_tracker, pa_odom);
      _pa.estimate();
    }
    return true;
  }

  void Mapper2D::finishMapping(){

    if (_first_vertex) {
      std::cerr << "Mapper was not started" << std::endl;
      return;
    }
    
    //Check last tracker info
    Eigen::Isometry2f global_t = _tracker->globalT();
    Eigen::Isometry2f relative_tracker_t = _previous_tracker_t.inverse() * global_t;
    Eigen::Matrix3f cov = _tracker->solver()->informationMatrix().inverse();
    _ca.Oplus(relative_tracker_t, cov);
    _previous_tracker_t = global_t;

    //Preparing cloud final data
    _current_cloud_trajectory->add(*_tracker->reference());
    _current_cloud_trajectory->setPose(global_t);

    //Place new vertex in the graph
    bool isFirstVertex = _graph_map->isFirstVertex();
    VertexSE2* previousVertex = _graph_map->currentVertex();
    Vector3f pose = t2v(global_t);
    _graph_map->addVertex(pose, _current_cloud_trajectory);
    std::cerr << "Adding vertex: " << _graph_map->currentVertex()->id() << " Pose: " << t2v(global_t).transpose() << endl;
    
    _graph_map->addEllipseData(_graph_map->currentVertex());

    if (isFirstVertex)
      return;

    //Get edge covariance from accumulator
    Eigen::Matrix3d inf = _ca.P().inverse().cast<double>();
    Eigen::Isometry2f delta_t = _previous_vertex_tracker_t.inverse() * global_t;
    SE2 relative_transf(delta_t.cast<double>());
    _graph_map->addEdge(previousVertex, _graph_map->currentVertex(), relative_transf, inf);

    //Trying to get some closures
    std::chrono::steady_clock::time_point time_matching_start = std::chrono::steady_clock::now();
    _graph_map->matchVertex(_graph_map->currentVertex());
    std::chrono::steady_clock::time_point time_matching_end = std::chrono::steady_clock::now();
    if (_verbose){
      std::cerr << "Matching Vertex took: "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(time_matching_end - time_matching_start).count() << " ms" << std::endl;
    }
    
    //Forcing verifying closures by changing closure window 
    std::cerr << "Last closures checking..." << std::endl;
    _graph_map->setLoopClosureWindow(1);
    _graph_map->checkClosures();

    //Optimizing
    std::chrono::steady_clock::time_point time_optimization_start = std::chrono::steady_clock::now();
    _graph_map->optimize(10);
    std::chrono::steady_clock::time_point time_optimization_end = std::chrono::steady_clock::now();
    if (_verbose){
      std::cerr << "Optimizing Graph took: "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(time_optimization_end - time_optimization_start).count() << " ms" << std::endl;
    }
    if (_log_times){
      *(_log_stream) << std::chrono::duration_cast<std::chrono::milliseconds>(time_optimization_end - time_optimization_start).count() << std::endl;

    }
    
    //Merging clouds
    std::cerr << "Last merging..." << _graph_map->verticesToMerge().size() << std::endl;
    
    if (_use_merger){
      std::cerr << std::endl << "MERGER" << std::endl;
      _gmerger.setGraphMap(_graph_map);
      _gmerger.setCloudMergerProjector(_tracker->projector());
      
      if (_graph_map->verticesToMerge().size()){
	for (std::set<OptimizableGraph::VertexSet>::iterator itvset = _graph_map->verticesToMerge().begin(); itvset != _graph_map->verticesToMerge().end(); itvset++){
	  OptimizableGraph::VertexSet vset = *itvset;
	  _gmerger.addVerticesToMerge(vset);
	}
      }
      _graph_map->closureBuffer().clear();
      _gmerger.triggerMerge();
      _graph_map->optimize(5);
    }

    _graph_map->updateEllipseData();

  }

  
  void Mapper2D::saveData(){
    std::chrono::steady_clock::time_point time_save_start = std::chrono::steady_clock::now();
    _graph_map->saveClouds();
    std::chrono::steady_clock::time_point time_save_end = std::chrono::steady_clock::now();
    if (_verbose){
      std::cerr << "Saving clouds took: "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(time_save_end - time_save_start).count() << " ms" << std::endl;
    }
  }

  void Mapper2D::clear(){
    tracker()->clear();
    tracker()->setProjector(NULL);
    graphMap()->clear();

    _previous_vertex_tracker_t = Eigen::Isometry2f::Identity();
    _previous_tracker_t = Eigen::Isometry2f::Identity();

    _ca.init();
    _pa.init();
    
    _track_new_cloud = true;
    
    _current_cloud_trajectory = NULL;

    _first_vertex = true;
  }

  void Mapper2D::init(LaserMessage* las, const Eigen::Isometry2f& init_pose){
    if (!_first_vertex){
      std::cerr << "Incorrect use of init. May you call clear() first? " << std::endl;
      return;
    }
    
    if (!las)
      return;

    if (_tracker == NULL) {
      std::cerr << "Need to initialize the tracker" << std::endl;
      return;
    }

    if (_graph_map == NULL) {
      std::cerr << "Need to initialize the graph" << std::endl;
      return;
    }

    //Add laser message to tracker
    if (!_tracker->compute(las))
      return;

    //Preparing cloud
    _current_cloud_trajectory = new Cloud2DWithTrajectory;
    _current_cloud_trajectory->setPose(init_pose);
    _current_cloud_trajectory->add(*_tracker->reference());
    LaserMessage* las2 = new LaserMessage;
    *las2 = *las;
    _current_cloud_trajectory->pushWaypoint(las2); //stores original laserscan (debugging)

    //Adding first vertex
    Vector3f pose = t2v(init_pose);
    _graph_map->addVertex(pose, _current_cloud_trajectory);
    std::cerr << "Adding vertex: " << _graph_map->currentVertex()->id() << " Pose: " << pose.transpose() << endl;
    
    _graph_map->addEllipseData(_graph_map->currentVertex());

    _previous_vertex_tracker_t = _tracker->globalT();
    _previous_tracker_t = _tracker->globalT();

    _first_vertex = false;

    Eigen::Isometry2f pa_estimate = init_pose;
    Eigen::Isometry2f pa_tracker = _previous_vertex_tracker_t;
    Eigen::Isometry2f pa_odom = toIsometry2f(las->odometry());
    _pa.setEstimate(pa_estimate, pa_tracker, pa_odom);
    _pa.estimate();
    
    if (_multi_session)
      _multi_session_started = false;
      
  }


  void Mapper2D::initMultiSession(const Eigen::Isometry2f& start_session_pose){
    
    //Look for near vertices from newly added vertex
    VertexSE2* currentVertex = _graph_map->currentVertex();
    currentVertex->setEstimate(SE2(start_session_pose.cast<double>()));
    currentVertex->setFixed(false);
    _current_cloud_trajectory->setPose(start_session_pose);
    
    VerticesFinder vf(_graph_map->graph());
    OptimizableGraph::VertexSet closevertices;
    vf.findVerticesEuclideanDistance(closevertices, currentVertex, 5);
    closevertices.erase(currentVertex);
    VertexSE2* closestv = (VertexSE2*) vf.findClosestVertex(closevertices, currentVertex);
    std::cerr << "Closest vertex = " << closestv->id() << std::endl;
    
    Cloud2DAligner c2da(_tracker->projector());
    c2da.useNNCorrespondenceFinder();
    c2da.setReference(_graph_map->vertexCloud(closestv));
    c2da.setCurrent(_current_cloud_trajectory);
    Eigen::Isometry2f init_guess = _graph_map->vertexCloud(closestv)->pose().inverse()*_current_cloud_trajectory->pose();
    c2da.compute(init_guess);
    
    SE2 relative_transf;
    Eigen::Matrix3d inf;
    if (c2da.validate()){
      //use result from alignment
      relative_transf = SE2(c2da.T().cast<double>());
      inf = c2da.alignInformationMatrix().cast<double>();
    }
    else{
      //use initial guess
      relative_transf = SE2(init_guess.cast<double>());
      inf = 100*Eigen::Matrix3d::Identity();
    }

    _graph_map->addEdge(closestv, _graph_map->currentVertex(), relative_transf, inf);

    _multi_session_started = true;
    std::cerr << std::endl << "Session started" << std::endl;
    
  }

  void Mapper2D::setLogTimes(bool log_times) {
    _log_times = log_times;
    if (_log_times) {
      if (_log_stream){
	_log_stream->close();
	delete _log_stream;
      }
      _log_stream = new ofstream("times_optimization.dat");
    } else {
      if (_log_stream){
	_log_stream->close();
	delete _log_stream;
      }
    }
  }
  
}
